package org.hansed.towerDefence

import org.hansed.engine.Engine
import org.hansed.engine.core.input.MousePicker
import org.hansed.towerDefence.ui.Scenes

/**
  * A 3D Tower Defence game
  *
  * @author Hannes Sederholm
  */
object TowerDefence extends App {

  Engine.init(windowWidth, windowHeight, "Tower Defence")

  Engine.scene = Scenes.START_SCENE

  //enable the mouse picker because we need that for building
  MousePicker.enabled = true

  Engine.run()
}
