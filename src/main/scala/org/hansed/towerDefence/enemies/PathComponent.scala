package org.hansed.towerDefence.enemies

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.components.Updater
import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.physics.movement.LinearSpeedMovement
import org.hansed.towerDefence.enemies.PathComponent._
import org.joml.Vector3f

import scala.collection.mutable

/**
  * Moves the parent object along a path defined by a buffer of coordinates.
  *
  * Works by deleting visited tiles from the path so makes a clone of the path on creation
  *
  * @param inputPath   buffer of tiles that are walkable
  * @param currentTile the starting tile, if this is in the middle of the path there is no guarantee of the direction
  *                    the movement will take. This means that in the context of a tower defence game this should be
  *                    the first tile on the path
  * @param speed       speed of movement
  * @author Hannes Sederholm
  */
class PathComponent(inputPath: mutable.Set[(Int, Int)], var currentTile: (Int, Int), val speed: () => Float)
  extends Updater {

  private val path = inputPath.clone()

  private var nextTile: Option[(Int, Int)] = None

  path -= currentTile

  /**
    * Called when the component is created. Should be used for initiating the component
    *
    * @param parent the parent object of this component
    */
  override def prepare(parent: GameObject): Unit = {
    findNextTile()
  }

  /**
    * Find the tile after newly reached tile and reset progress
    * Only allowed to pick tiles right next to the current one, so no diagonal movement
    *
    * @return tile in Option or None if at the end of the path
    */
  def findNextTile(): Unit = {
    val prevTile = currentTile
    val lastTile = nextTile.getOrElse(currentTile)
    nextTile = path.find(p => {
      val offsetX = Math.abs(p._1 - lastTile._1)
      val offsetY = Math.abs(p._2 - lastTile._2)
      (offsetX == 1 && offsetY == 0) || (offsetX == 0 && offsetY == 1)
    })
    nextTile.foreach(path -= _)
    currentTile = lastTile
    parent.foreach(p => {
      if (nextTile.isEmpty) {
        Events.signal(PathFinished(p))
        remove()
      } else {
        parent.foreach(p => {
          val targetTile: Vector3f = new Vector3f(currentTile._1, position.y, currentTile._2)
          p.addComponent(new LinearSpeedMovement(speed(), () => targetTile, onTarget = () => findNextTile()))
        })
        Events.signal(PathUpdate(p, prevTile._1, prevTile._2))
      }
    })
  }

  override def update(): Unit = {}
}

object PathComponent {

  /**
    * Sent when a path component makes progress is reaching its goal
    *
    * @param gameObject object that the component is attached to
    * @param newX       new tiles x
    * @param newY       new tiles y
    */
  case class PathUpdate(gameObject: GameObject, newX: Int, newY: Int) extends Event

  /**
    * Sent when a path component has escorted an object all the way to the end of the path
    *
    * @param gameObject the object attached to the component
    */
  case class PathFinished(gameObject: GameObject) extends Event

}

