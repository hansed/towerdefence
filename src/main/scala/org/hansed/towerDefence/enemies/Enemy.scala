package org.hansed.towerDefence.enemies

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.{Clickable, EventListener, Events}
import org.hansed.engine.rendering.models.Model
import org.hansed.engine.rendering.ui.info.StatusBar
import org.hansed.towerDefence.MoneyComponent.MoneyIncreased
import org.hansed.towerDefence.data.EnemyDefinition
import org.hansed.towerDefence.data.SaveProtocol.EnemyDefinitionLoader
import org.hansed.towerDefence.events.{DamageEvent, DeathEvent}
import org.hansed.util.Debug

/**
  * Represents an enemy in the game world
  *
  * @param definitionName the definition of this type of enemy stored on disk
  * @author Hannes Sederholm
  */
class Enemy(definitionName: String) extends GameObject with EventListener with Clickable {

  /**
    * The config that defines this enemy
    */
  val definition: EnemyDefinition = EnemyDefinitionLoader.config(definitionName)

  Debug.logTime(() => {
    addChild(new Model(definition.model))
    addChild(new StatusBar(() => health, definition.health, () => this.position))
  }, "adding components", this)


  /**
    * Current health
    */
  var health: Float = definition.health


  /**
    * Reduce the health of this enemy
    *
    * @param damage amount of damage to take
    */
  def takeDamage(damage: Float): Unit = {
    health -= damage
    if (dead) {
      remove()
      //give 5 coins / enemy killed
      Events.signal(MoneyIncreased(5))
      Events.signal(DeathEvent(this))
    }
  }

  /**
    * Gets the defined damage for this enemy
    *
    * @return damage dealt by this enemy if passed
    */
  def damage: Int = definition.damage

  /**
    * Is this enemy dead yet?
    *
    * @return if dead
    */
  def dead: Boolean = health <= 0

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      //look for ways to get hit
      case event: DamageEvent => if (event.target.equals(this)) takeDamage(event.damage)
      case _ =>
    }
  }

}
