package org.hansed.towerDefence.ui

import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.info.Label
import org.hansed.towerDefence.Level.WaveUpdate

class WaveDisplay(_x: Int, _y: Int, w: Int, h: Int) extends Label("", _x, _y, w, h) with EventListener {

  background = UI.primaryColor

  /**
    * Deal with an event
    *
    * @param e an event sent by an EventDispatcher
    */
  override def processEvent(e: Event): Unit = {
    e match {
      case event: WaveUpdate =>
        text = "Wave: " + event.wave
      case _ =>
    }
  }
}
