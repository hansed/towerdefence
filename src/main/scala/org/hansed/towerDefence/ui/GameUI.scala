package org.hansed.towerDefence.ui

import org.hansed.engine.Flags
import org.hansed.engine.Flags.GAME_OVER
import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.rendering.textures.SpriteSheet
import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.containers.{ListBox, Panel}
import org.hansed.engine.rendering.ui.info.{Icon, Label}
import org.hansed.engine.util.interpolation.Interpolator
import org.hansed.towerDefence.Level.LevelEnd
import org.hansed.towerDefence.MoneyComponent.Transaction
import org.hansed.towerDefence.data.SaveProtocol.TowerDefinitionLoader
import org.hansed.towerDefence.events.{AddDefender, BeginSellTower}

/**
  * The ui displayed when the player is playing the game
  * Consists of various components containing information about the state of the game
  * The top components contain information about the game state like money, lives, current wave, etc.
  * Bottom panel contains controls for the player to select towers etc
  *
  * @author Hannes Sederholm
  */
class GameUI(w: Int, h: Int) extends UI(0, 0, w, h) {

  final val COMPONENT_HEIGHT = 30
  final val BOTTOM_PANEL_HEIGHT = 100
  final val SIDE_BAR_WIDTH = 50

  val iconSpriteSheet: SpriteSheet =
    new SpriteSheet("ui/MeteorRepository1Icons_16x16.png", 16, 16, 12,
      1, 1)

  add(new WaveDisplay((w - 150) / 2, 20, 150, COMPONENT_HEIGHT))
  add(new LifeDisplay(w - 100, 20, 80, COMPONENT_HEIGHT))

  addChild(new EnemyViewer)

  add(new Icon("game/ui/icons/tilted_cross.png", 20, 20, 20, 20) {
    override def onClick(buttonId: Int): Boolean = {
      if (!Flags(GAME_OVER))
        Events.signal(LevelEnd())
      true
    }
  })

  //SIDEBAR
  add(new Panel(w - SIDE_BAR_WIDTH, h / 4, SIDE_BAR_WIDTH, h / 2) {
    add(new ListBox(8, 10, width - 16, height - 20, SIDE_BAR_WIDTH - 16, margin = 10, items = Vector(
      new Icon(iconSpriteSheet(68), 0, 0, 0, 0) {
        setTooltip("Sell tower", UI.UI_ALIGN_LEFT)

        /**
          * Called if this component is clicked and none of the children consume the event
          *
          * The event can be consumed by returning true with this method
          *
          * @param button the button clicked
          * @return
          */
        override protected def onClick(button: Int): Boolean = {
          Events.signal(BeginSellTower())
          true
        }
      },
      new Icon(iconSpriteSheet(61), 0, 0, 0, 0) {
        setTooltip("Send defender $ 10", UI.UI_ALIGN_LEFT)

        override protected def onClick(button: Int): Boolean = {
          Events.signal(new Transaction(10) {
            override def onSuccess(): Unit = {
              Events.signal(AddDefender())
            }

            override def onFailure(): Unit = {
              val notification = new Label("Not enough money!", -175, 0, 165, 30) {
                background = UI.primaryColor
              }
              add(notification)
              Interpolator.animate(0, 1.0f, (p: Float) => {
                val beforeArea = notification.area
                notification.y = (0 - (Math.max(0, p) * 30.0f)).toInt
                repaint(beforeArea.union(notification.area))
              }, 500, onFinish = () => {
                remove(notification)
              })
            }
          })
          true
        }
      }
    )))
  })

  add(new MoneyDisplay(w - 100, COMPONENT_HEIGHT + 40, 80, COMPONENT_HEIGHT))

  private val bottomPanel = new Panel(0, h - BOTTOM_PANEL_HEIGHT, w, BOTTOM_PANEL_HEIGHT)

  add(bottomPanel)

  val modelView = new GameObject()


  private val towerDefinitions = Math.max(TowerDefinitionLoader.configs.size, 1)
  private val towerDefWidth = Math.min(w / towerDefinitions, 300)

  TowerDefinitionLoader.configNames.map(TowerDefinitionLoader.config).toSeq.sortBy(_.price).zipWithIndex.foreach(p => {
    bottomPanel.add(new TowerDisplay(p._1, towerDefWidth * p._2, 0, towerDefWidth, BOTTOM_PANEL_HEIGHT))
  })

}
