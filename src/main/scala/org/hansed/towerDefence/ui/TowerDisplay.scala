package org.hansed.towerDefence.ui

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.rendering.models.Model
import org.hansed.engine.rendering.ui.containers.{ModelView, Panel}
import org.hansed.engine.rendering.ui.info.{Icon, Label, ProgressBar}
import org.hansed.towerDefence.data.TowerDefinition
import org.hansed.towerDefence.events.SelectTower
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW._

/**
  * A panel that display information about a tower and allows the player to click on it to add it to the map
  *
  * @param definition the configs of the displayed tower
  * @param x          x coordinate of top right corner
  * @param y          y coordinate of top right corner
  * @param width      width
  * @param height     height
  * @author Hannes Sederholm
  */
class TowerDisplay(definition: TowerDefinition, x: Int, y: Int, width: Int = 200, height: Int = 80)
  extends Panel(x, y, width, height) {

  private val margin = 5

  private val controlSize = 20

  private val barHeight = (height - margin * 5) / 4

  private val barWidth = width - height - controlSize - margin * 4

  private def createBar(index: Int, text: String, value: Float, max: Float = 1.0f) = {
    val bar = new ProgressBar(height + margin, margin * index + barHeight * index, barWidth, barHeight, max = max, text = text)
    bar.value = value
    bar
  }

  private def createControl(index: Int, iconPath: String, action: () => Unit): Icon = {
    new Icon(iconPath, height + barWidth + margin * 2, margin * index + controlSize * (index - 1), controlSize, controlSize) {
      override def onClick(buttonId: Int): Boolean = {
        if (buttonId == GLFW_MOUSE_BUTTON_LEFT) {
          action()
        }
        true
      }
    }
  }

  add(createBar(1, "Range", definition.range, 10))
  add(createBar(2, "Damage", definition.damage, 10))
  add(createBar(3, "Speed", definition.speed))

  add(createControl(1, "game/ui/icons/face_on_plus_health.png", () => {
    Events.signal(SelectTower(definition))
  }))

  add(new Label(definition.name + "   $" + definition.price, height, 0, width - height - controlSize - margin * 2, barHeight + margin,
  ))

  add(new ModelView(0, 0, height, height, new GameObject() {
    addChild(new Model(definition.model) {
      position = new Vector3f(0.0f, -0.5f, 0.0f)
    })
  }))

}
