package org.hansed.towerDefence.ui

import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.info.IconLabel
import org.hansed.towerDefence.Level.LivesUpdate

/**
  * An UI element that displays the number of lives the player has left
  *
  * @author Hannes Sederholm
  */
class LifeDisplay(x: Int, y: Int, width: Int, height: Int)
  extends IconLabel("game/ui/icons/face_on_heart.png", "", x, y, width, height, iconPadding = 5)
    with EventListener {

  background = UI.primaryColor

  /**
    * Deal with an event
    *
    * @param e an event sent by an EventDispatcher
    */
  override def processEvent(e: Event): Unit = {
    e match {
      case event: LivesUpdate =>
        text = "" + event.lives
      case _ =>
    }
  }
}
