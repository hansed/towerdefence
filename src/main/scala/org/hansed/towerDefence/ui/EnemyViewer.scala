package org.hansed.towerDefence.ui

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.HierarchyEvents.ObjectRemoved
import org.hansed.engine.core.eventSystem.events.InputEvents.ObjectClick
import org.hansed.engine.rendering.models.Model
import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.containers.{ModelView, Panel}
import org.hansed.engine.rendering.ui.info.{Label, ProgressBar}
import org.hansed.engine.rendering.ui.input.Button
import org.hansed.towerDefence.data.EnemyDefinition
import org.hansed.towerDefence.enemies.Enemy

/**
  * Listens for clicks on enemies and displays their information in a closable panel when they are clicked
  *
  * @author Hannes Sederholm
  */
class EnemyViewer extends GameObject with EventListener {

  private var visible = false

  private var healthBar: ProgressBar = _
  private var damageBar: ProgressBar = _
  private var speedBar: ProgressBar = _
  private var nameLabel: Label = _
  private var modelView: ModelView = _

  private var view: UI = _

  private val (viewWidth, viewHeight) = (220, 85)

  private var definition: EnemyDefinition = _

  {
    val margin = 5
    val controlSize = 20

    val barHeight = (viewHeight - margin * 6) / 3
    val barWidth = viewWidth - viewHeight - controlSize - margin * 4

    def createBar(index: Int, text: String, value: Float, max: Float = 1.0f): ProgressBar = {
      new ProgressBar(viewHeight + margin, margin + barHeight * index, barWidth, barHeight, max = max, text = text)
    }

    healthBar = createBar(1, "Health", 0, max = 10)
    damageBar = createBar(2, "Damage", 0, max = 5)
    speedBar = createBar(3, "Speed", 0, max = 5)

    nameLabel = new Label("", viewHeight, 0, viewWidth - viewHeight - controlSize - margin * 2, barHeight + margin)

    modelView = new ModelView(0, 0, viewHeight, viewHeight, new Model("cube"))

    view = new Panel(150, 20, viewWidth, viewHeight) {
      add(healthBar, damageBar, speedBar, nameLabel, modelView)
      add(new Button("x", controlSize, controlSize, height + barWidth + margin * 2, margin) {
        override def onClick(buttonId: Int): Boolean = {
          parent.foreach(_.remove())
          true
        }
      })
    }.asUI

  }

  def updateData(definition: EnemyDefinition): Unit = {
    if (this.definition != definition) {
      this.definition = definition
      modelView.parentComponent.foreach(parent => {
        modelView.renderer = new Model(definition.model)
        healthBar.value = definition.health
        damageBar.value = definition.damage
        speedBar.value = definition.speed
        nameLabel.text = definition.name
      })
    }
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: ObjectClick =>
        event.gameObject match {
          case enemy: Enemy =>
            if (!visible) {
              visible = true
              addChild(view)
            }
            updateData(enemy.definition)
          case _ =>
        }
      case event: ObjectRemoved =>
        if (event.childObject.equals(this.view)) {
          visible = false
        }
      case _ =>
    }
  }
}
