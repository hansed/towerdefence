package org.hansed.towerDefence.ui

import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.info.IconLabel
import org.hansed.towerDefence.MoneyComponent.MoneyChanged

class MoneyDisplay(x: Int, y: Int, width: Int, height: Int)
  extends IconLabel("game/ui/icons/face_on_gold_coin.png", "", x, y, width, height, iconPadding = 5)
    with EventListener {

  background = UI.primaryColor

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: MoneyChanged =>
        text = event.newAmount.toString
      case _ =>
    }
  }
}
