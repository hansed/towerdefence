package org.hansed.towerDefence.ui

import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.input.MousePicker
import org.hansed.engine.core.{GameObject, GameScene}
import org.hansed.engine.rendering.camera.{Camera, EntityFollowingCamera, SkyboxComponent}
import org.hansed.engine.rendering.ui._
import org.hansed.engine.rendering.ui.containers.ListBox
import org.hansed.engine.rendering.ui.info.TextView
import org.hansed.engine.rendering.ui.input.Button
import org.hansed.engine.{Engine, Flags}
import org.hansed.towerDefence.Level.LevelEnd
import org.hansed.towerDefence.data.SaveProtocol.LevelDefinitionLoader
import org.hansed.towerDefence.{Level, windowHeight, windowWidth}
import org.hansed.util.Debug
import org.joml.Matrix4f

import scala.collection.mutable

/**
  * Contains all the scenes in the game
  *
  * @author Hannes Sederholm
  */
object Scenes {

  private val windowSize: (Int, Int) = Engine.windowSize

  val GAME_CAMERA: Camera = new EntityFollowingCamera(new Matrix4f().perspective(Math.toRadians(45.0).toFloat, windowWidth
    .toFloat / windowHeight, 0.01f, 500.0f), radius = 50.0f, controllable = true)

  GAME_CAMERA.addComponent(new SkyboxComponent("default"))

  val MENU_CAMERA: Camera = new Camera(new Matrix4f().perspective(Math.toRadians(45.0).toFloat, windowWidth.toFloat /
    windowHeight, 0.01f, 500.0f))

  MENU_CAMERA.addComponent(new SkyboxComponent("default"))

  lazy val GAME_SCENE: GameScene = GameScene(GAME_CAMERA, new GameObject() with EventListener {
    addChild(new GameUI(windowWidth, windowHeight))
    addComponent(MousePicker)

    override def processEvent(event: Event): Unit = event match {
      case _: LevelEnd =>
        Engine.scene = START_SCENE
      case _ =>
    }
  })

  private val MENU_WIDTH = windowWidth / 2
  private val MENU_X = MENU_WIDTH / 2
  private val MENU_Y = windowHeight / 6
  private val MENU_HEIGHT = windowHeight - MENU_Y * 2

  def createButton(data: (String, () => Unit)): Button = {
    new Button(data._1, 0, 0, 0, 0) {
      override protected def onClick(button: Int): Boolean = {
        data._2()
        true
      }
    }
  }

  lazy val levelMenuStructure: mutable.Buffer[(String, () => Unit)] = LevelDefinitionLoader.configNames
    .map(key => (LevelDefinitionLoader.config(key).name, () => {
      Engine.scene = GAME_SCENE
      Debug.logAction(() => GAME_SCENE.rootObject.addChild(new Level(key)), "Loading level " + key, this)
    })).toBuffer


  lazy val LEVEL_SELECT: GameScene = GameScene(MENU_CAMERA, new UI(MENU_X, MENU_Y, MENU_WIDTH, MENU_HEIGHT) {
    add(new ListBox(0, 0, MENU_WIDTH, MENU_HEIGHT, items = (levelMenuStructure ++
      mutable.Buffer(("Back", () => Engine.scene = START_SCENE))).map(createButton).toVector, margin = 50, preferredRowHeight = 80))
  })

  val CREDITS_SCENE: GameScene = GameScene(MENU_CAMERA, new UI(0, 0, windowWidth, windowHeight) {
    add(new TextView(
      "Credits:\n\n\n" +
        "The font: Neuropol x free " +
        "\n     from http://typodermicfonts.com/\n" +
        "\n Icons by www.gamedeveloperstudio.com\n" +
        "\n Textures from: https://opengameart.org/users/spiney\n" +
        "\n Progress bar by https://opengameart.org/users/arkalain\n" +
        "\n Normal plane model by https://opengameart.org/users/wobba89\n" +
        "\n Stealth plane by Eric Buisson, downloaded from opengameart.org\n" +
        "\n Building area tool icon from https://www.freeiconspng.com/img/12286\n", 0, 175, windowWidth, windowHeight - 150) {
      foreground = UI.primaryColor
    })
    add(new Button("Back", 160, 60, windowWidth / 2 - 80, windowHeight - 100) {
      override protected def onClick(button: Int): Boolean = {
        Engine.scene = START_SCENE
        true
      }
    })
  })

  val START_SCENE: GameScene = GameScene(MENU_CAMERA, new UI(MENU_X, MENU_Y, MENU_WIDTH, MENU_HEIGHT) {
    add(new ListBox(0, 0, MENU_WIDTH, MENU_HEIGHT, items = Vector(
      ("Play", () => Engine.scene = Scenes.LEVEL_SELECT),
      ("Credits", () => Engine.scene = Scenes.CREDITS_SCENE),
      ("Quit", () => Flags(Flags.SHUTDOWN) = true)).map(createButton), preferredRowHeight = 80, margin = 50))
  })

}
