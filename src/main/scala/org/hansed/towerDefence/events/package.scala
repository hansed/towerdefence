package org.hansed.towerDefence

import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.towerDefence.data.TowerDefinition
import org.hansed.towerDefence.enemies.Enemy
import org.hansed.towerDefence.towers.BuildArea

/**
  * Contains various event definitions specific to the tower defence game
  *
  * @author Hannes Sederholm
  */
package object events {

  /**
    * Sent when the player wants to add a defender to the path
    */
  case class AddDefender() extends Event

  /**
    * Sent when the player clicks a build area
    *
    * @param buildArea the build area clicked
    */
  case class AttemptBuildTower(buildArea: BuildArea) extends Event

  /**
    * Sent when the player clicks somewhere other than a build area
    */
  case class ClearSelectedTower() extends Event

  /**
    * Sent when an enemy is damaged
    *
    * @param damage the amount of damage taken
    * @param target the enemy taking the damage
    */
  case class DamageEvent(damage: Float, target: Enemy) extends Event

  /**
    * Sent when the player selects a tower to build via the add button on the tower display at the bottom of the screen
    *
    * @param definition of the tower selected
    */
  case class SelectTower(definition: TowerDefinition) extends Event


  /**
    * Sent when an enemy dies
    *
    * @param enemy the dead enemy
    */
  case class DeathEvent(enemy: Enemy) extends Event

  /**
    * Sent when the user clicks the sell button on the side bar
    */
  case class BeginSellTower() extends Event

  /**
    * Sent when a tower is clicked after selling has been started
    */
  case class FinishSellTower() extends Event

}
