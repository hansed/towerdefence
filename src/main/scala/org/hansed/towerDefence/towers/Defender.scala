package org.hansed.towerDefence.towers

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.components.Updater
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.rendering.models.Model
import org.hansed.towerDefence.Level
import org.hansed.towerDefence.data.EnemyDefinition
import org.hansed.towerDefence.data.SaveProtocol.EnemyDefinitionLoader
import org.hansed.towerDefence.enemies.PathComponent
import org.hansed.towerDefence.enemies.PathComponent.PathFinished
import org.hansed.towerDefence.events.DamageEvent
import org.joml.Vector3f

import scala.collection.mutable

/**
  * Represents a thing that walks on the path towards the enemies and damages them if it finds any
  * Uses the same definitions as enemies, but in a different manner
  *
  * @author Hannes Sederholm
  */
class Defender(definitionName: String, path: mutable.Set[(Int, Int)], startTile: (Int, Int)) extends GameObject with
  EventListener {

  val definition: EnemyDefinition = EnemyDefinitionLoader.config(definitionName)

  val tracker = new EnemyTracker(1.0f)

  addComponent(tracker)

  position = new Vector3f(startTile._1 * Level.TILE_SIZE, 0, startTile._2 * Level.TILE_SIZE)

  addComponent(new PathComponent(path, startTile, () => definition.speed))

  addComponent(new Updater {
    override def update(): Unit = {
      tracker.findAny.foreach(target => {
        Events.signal(DamageEvent(definition.health, target))
        Defender.this.remove()
      })
    }
  })

  addChild(new Model(definition.model))

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: PathFinished =>
        if (event.gameObject.uniqueId == uniqueId)
          remove()
      case _ =>
    }
  }
}
