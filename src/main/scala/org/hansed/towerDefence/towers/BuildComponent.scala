package org.hansed.towerDefence.towers

import org.hansed.engine.Flags
import org.hansed.engine.Flags.GAME_OVER
import org.hansed.engine.core.components.SimpleComponent
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.towerDefence.MoneyComponent._
import org.hansed.towerDefence.data.TowerDefinition
import org.hansed.towerDefence.events.{AttemptBuildTower, BeginSellTower, ClearSelectedTower, SelectTower}

/**
  * Deals with building towers onto the map
  *
  * @author Hannes Sederholm
  */
class BuildComponent extends SimpleComponent with EventListener {

  var selectedTower: Option[TowerDefinition] = None
  var startedBuild: Option[TowerDefinition] = None
  var targetArea: Option[BuildArea] = None

  /**
    * Reset everything about the current attempted build, so that we are ready for a new attempt
    */
  private def resetBuild(): Unit = {
    selectedTower = None
    startedBuild = None
    targetArea = None
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    if (!Flags(GAME_OVER))
      event match {
        case event: AttemptBuildTower =>
          selectedTower.foreach(definition => {
            startedBuild = Some(definition)
            targetArea = Some(event.buildArea)
            Events.signal(new Transaction(definition.price) {
              override def onSuccess(): Unit = {
                targetArea.foreach(target => {
                  target.parent.foreach(parent => {
                    startedBuild.foreach(definition => {
                      parent.addChild(new Tower(config = Some(definition)) {
                        position = target.position
                      })
                    })
                  })
                  target.remove()
                })
                resetBuild()
              }

              override def onFailure(): Unit = {
                resetBuild()
              }
            })
          })
        case _: ClearSelectedTower =>
          selectedTower = None
        case event: SelectTower =>
          selectedTower = Some(event.definition)
        case _: BeginSellTower =>
          Events.signal(ClearSelectedTower(), instantly = true)
        case _ =>
      }
  }
}
