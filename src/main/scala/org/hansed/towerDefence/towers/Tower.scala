package org.hansed.towerDefence.towers

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.ObjectClick
import org.hansed.engine.core.eventSystem.{Clickable, EventListener, Events}
import org.hansed.engine.rendering.models.Model
import org.hansed.engine.rendering.models.animated.Animator
import org.hansed.engine.rendering.ui.info.StatusBar
import org.hansed.towerDefence.MoneyComponent.Transaction
import org.hansed.towerDefence.data.SaveProtocol.TowerDefinitionLoader
import org.hansed.towerDefence.data.TowerDefinition
import org.hansed.towerDefence.events.{BeginSellTower, FinishSellTower, SelectTower}
import org.joml.Vector3f

/**
  * Represents a tower in the world
  *
  * @param definitionName defines this tower
  * @param config         allows you to specify the definition to use instead of the name,
  *                       completely ignores the name parameter when defined
  * @author Hannes Sederholm
  */
class Tower(val definitionName: String = "basic_tower", config: Option[TowerDefinition] = None)
  extends GameObject with Clickable with EventListener {

  val definition: TowerDefinition = config.getOrElse(TowerDefinitionLoader.config(definitionName))

  //the point that the towers projectiles will start from
  private val firingPosition = new Vector3f(definition.firingPosition(0), definition.firingPosition(1),
    definition.firingPosition(2))

  val model = new Model(definition.model)

  addChild(model)

  private val enemyTracker = new EnemyTracker(definition.range)

  addComponent(enemyTracker)

  //makes the tower fire at enemies
  val firingComponent = new FiringComponent(() => firingPosition.add(position, new Vector3f()),
    definition.speed, definition.damage, definition.projectile, definition.rotateToFire, () => enemyTracker.findAny)
  addComponent(firingComponent)

  addChild(new StatusBar(() => firingComponent.loadingProgress, 1.0f, () => position))

  //sets the possible animators to not play while nothing is happening
  onComponents {
    case animator: Animator =>
      animator.playing = false
      animator.autoRepeat = false
    case _ =>
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case _: BeginSellTower =>
        model.setHighlight(true)
      case _: SelectTower | _: FinishSellTower =>
        model.setHighlight(false)
      case event: ObjectClick =>
        if (event.gameObject.equals(this) && model.isHighlighted) {
          Events.signal(FinishSellTower(), instantly = true)
          Events.signal(new Transaction(-definition.price) {
            /**
              * Called if the transaction was successful
              */
            override def onSuccess(): Unit = {
              remove()
            }

            /**
              * Called if the transaction wasn't successful
              */
            override def onFailure(): Unit = {
              throw new IllegalStateException("Tower should always be sellable")
            }
          })
        }
      case _ =>
    }
  }
}
