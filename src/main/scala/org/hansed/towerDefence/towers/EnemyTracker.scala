package org.hansed.towerDefence.towers

import org.hansed.engine.core.components.SimpleComponent
import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.HierarchyEvents.{ObjectAdded, ObjectRemoved}
import org.hansed.towerDefence.enemies.Enemy
import org.hansed.towerDefence.enemies.PathComponent.{PathFinished, PathUpdate}

import scala.collection.mutable

/**
  * A component that tracks enemies within specified range of the parent
  *
  * @param range range to enemies
  * @author Hannes Sederholm
  */
class EnemyTracker(range: Float) extends SimpleComponent with EventListener {

  val targets: mutable.HashSet[Enemy] = mutable.HashSet[Enemy]()

  /**
    * Find any target
    *
    * @return Some(Enemy) if there was a valid target or None if none was found
    */
  def findAny: Option[Enemy] = targets.find(inRange)

  /**
    * Calculates the distance between this tower and an enemy and returns if the distance is within range
    *
    * @param enemy the enemy
    * @return whether enemy is in range
    */
  def inRange(enemy: Enemy): Boolean = {
    enemy.position.distance(position) <= range
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: ObjectAdded =>
        event.childObject match {
          case enemy: Enemy =>
            targets += enemy
          case _ =>
        }
      case event: ObjectRemoved =>
        event.childObject match {
          case enemy: Enemy =>
            targets -= enemy
          case _ =>
        }
      case event: PathUpdate =>
        event.gameObject match {
          case enemy: Enemy =>
            targets += enemy
          case _ =>
        }
      case event: PathFinished =>
        event.gameObject match {
          case enemy: Enemy => targets -= enemy
          case _ =>
        }
      case _ =>
    }
  }

}
