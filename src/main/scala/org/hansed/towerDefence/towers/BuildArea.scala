package org.hansed.towerDefence.towers

import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.ObjectClick
import org.hansed.engine.core.eventSystem.{Clickable, EventListener, Events}
import org.hansed.engine.rendering.models.Model
import org.hansed.towerDefence.events.{AttemptBuildTower, SelectTower}

/**
  * Represents an object that can be replaced with a tower
  *
  * @param configName the name of the model config for this spot
  * @author Hannes Sederholm
  */
class BuildArea(configName: String) extends Model(configName) with EventListener with Clickable {

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: ObjectClick =>
        if (event.gameObject.equals(this)) {
          Events.signal(AttemptBuildTower(this))
        }
      case _: AttemptBuildTower =>
        setHighlight(false)
      case _: SelectTower =>
        setHighlight(true)
      case _ =>
    }
  }

}
