package org.hansed.towerDefence.towers

import org.hansed.engine.Engine
import org.hansed.engine.core.components.Updater
import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.physics.Projectile
import org.hansed.engine.physics.movement.LinearSpeedMovement
import org.hansed.engine.rendering.models.animated.Animator
import org.hansed.towerDefence.enemies.Enemy
import org.hansed.towerDefence.events.DamageEvent
import org.joml.Vector3f

/**
  * Fires projectiles at enemies within the range
  *
  * @param firingPosition    the starting position of the projectiles
  * @param speed             firing speed
  * @param damage            the damage dealt to the target
  * @param projectileConfig  projectiles model config name
  * @param faceTargetToShoot should the parent be made to face the target when firing
  * @author Hannes Sederholm
  */
class FiringComponent(val firingPosition: () => Vector3f, val speed: Float, val damage: Float,
                      val projectileConfig: String, val faceTargetToShoot: Boolean,
                      val targetProvider: () => Option[Enemy]) extends Updater {
  /**
    * How much does the tower progress loading / tick
    */
  val loadPerTick: Float = speed * Engine.GAME_TICK / Engine.UNIT

  /**
    * How far to loading the tower is, fires at 1.0 and drops by 1.0
    */
  var loadingProgress: Float = 1.0f

  /**
    * Called once every update tick for all components
    */
  override def update(): Unit = {
    if (loadingProgress > 1) {
      if (fire())
        loadingProgress -= 1.0f
    } else
      loadingProgress += loadPerTick
  }

  /**
    * Fire at a random enemy in range enemy
    *
    * @return If successful (if found an enemy in range)
    */
  def fire(): Boolean = {
    val targetOption = targetProvider()
    if (targetOption.isEmpty) {
      false
    } else {
      targetOption.foreach(target => {
        val projectile = new Projectile(projectileConfig, new LinearSpeedMovement(5.0f, () => target.position,
          onTarget = () => {
            Events.signal(DamageEvent(damage, target))
          })) {
          position = firingPosition()
        }
        parent.foreach(p => {
          if (faceTargetToShoot)
            p.lookAlong(target.position.sub(position, new Vector3f()))
          p.onComponents {
            case animator: Animator => animator.playOnce()
            case _ =>
          }
        })
        Engine.scene.rootObject.addChild(projectile)
      })
      true
    }
  }
}
