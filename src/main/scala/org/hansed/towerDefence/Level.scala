package org.hansed.towerDefence

import org.hansed.engine.Flags
import org.hansed.engine.Flags.GAME_OVER
import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.HierarchyEvents.ObjectRemoved
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.rendering.RenderingEvents.FocusEntityCamera
import org.hansed.engine.rendering.lights.PointLight
import org.hansed.engine.rendering.models.Model
import org.hansed.engine.rendering.ui.info.MessagePopup
import org.hansed.towerDefence.Level._
import org.hansed.towerDefence.data.SaveProtocol.{LevelDefinitionLoader, MapLoader}
import org.hansed.towerDefence.data.{CoordVal, LevelDefinition, MapDefinition, Wave}
import org.hansed.towerDefence.enemies.PathComponent._
import org.hansed.towerDefence.enemies.{Enemy, PathComponent}
import org.hansed.towerDefence.events.{AddDefender, DeathEvent}
import org.hansed.towerDefence.towers.{BuildArea, BuildComponent, Defender, Tower}
import org.hansed.util.Debug
import org.joml.Vector3f

import scala.collection.mutable

/**
  * The in game representation of a level
  *
  * Loads and adds the required enemies and objects automatically on creation
  *
  * @param definitionName name of the config file that describes this level
  * @author Hannes Sederholm
  */
class Level(definitionName: String) extends GameObject with EventListener {

  val definition: LevelDefinition = LevelDefinitionLoader.config(definitionName)

  /**
    * The number of lives left, if this goes to 0 the game ends
    */
  private var _lives: Int = definition.lives

  //let everything know the starting lives
  Events.signal(LivesUpdate(_lives))

  //stores the currency gained in this level
  addComponent(new MoneyComponent)
  addComponent(new BuildComponent)

  /**
    * Information about the map used for this level
    */
  val map: MapDefinition = MapLoader.config(definition.map)

  /**
    * The tile that the enemies spawn at
    */
  val startingTile: CoordVal = map.pathPoints.head

  /**
    * The tile that the defenders spawn at
    */
  val endingTile: CoordVal = map.pathPoints.last

  /**
    * The starting point for enemies as a world coordinate
    */
  val startingPosition: Vector3f = new Vector3f(startingTile.x * TILE_SIZE, 0.0f, startingTile.y * TILE_SIZE)

  /**
    * Upcoming waves
    */
  val waves: Iterator[Wave] = definition.waves.iterator

  /**
    * Number of wave
    */
  var waveIndex: Int = 0

  /**
    * The wave currently commencing
    */
  var currentWave: Wave = _

  /**
    * The path that the enemies may walk
    */
  var path: mutable.Set[(Int, Int)] = mutable.Set[(Int, Int)]()

  /**
    * Enemies currently on the area, if this goes empty we send another wave
    */
  val enemies: mutable.Set[Enemy] = mutable.Set()

  /**
    * The enemies yet to be added from this wave
    */
  var enemyQueue: mutable.Buffer[Enemy] = mutable.Buffer()

  Debug.logAction(() => {
    //fills the paths buffer
    constructPaths(map.pathPoints)
  }, "constructing paths", this)

  Debug.logAction(() => {
    //adds a grid of slab objects
    buildMapArea()
  }, "building map area", this)


  Debug.logAction(() => {
    //adds the tower building areas
    map.buildAreas.foreach(coordVal => {
      val renderer = new BuildArea(coordVal.value)
      renderer.position = new Vector3f(coordVal.x * TILE_SIZE, 0, coordVal.y * TILE_SIZE)
      addChild(renderer)
    })
  }, "adding building areas", this)

  /**
    * The latest enemy sent
    */
  var lastEnemy: Enemy = _

  currentWave = nextWave()

  /**
    * Check if its time to start the next wave and start it if it is
    * Called when the enemies on the map change
    */
  def checkNextWave(): Unit = {
    if (enemies.isEmpty) {
      currentWave = nextWave()
    }
  }

  /**
    * Create a grid of slabs according to the map definitions width and height
    */
  private def buildMapArea(): Unit = {
    for {
      x <- 0 until map.width
      y <- 0 until map.height
    } {
      if (path.contains((x, y))) {
        addChild(new Model("path") {
          position = new Vector3f(x * TILE_SIZE, 0, y * TILE_SIZE)
        })
      } else {
        addChild(new Model("slab") {
          position = new Vector3f(x * TILE_SIZE, 0, y * TILE_SIZE)
        })
      }
    }

    val cameraTarget = new GameObject() {
      position = new Vector3f(map.width / 2.0f * TILE_SIZE, 3.0f, map.height / 2.0f * TILE_SIZE)
      addChild(PointLight(new Vector3f(0.6f), power = 250.5f, offset = new Vector3f(0, 25, 0)))
    }
    addChild(cameraTarget)

    Events.signal(FocusEntityCamera(cameraTarget, autoUpdate = false))
  }

  /**
    * Send the next enemy in the enemyQueue buffer
    */
  private def sendNextEnemy(): Enemy = {
    val obj: Enemy = enemyQueue.remove(0)
    enemies.add(obj)
    addChild(obj)
    obj
  }

  /**
    * Load and prepare the enemy objects for all the enemies on this wave
    *
    * @param enemies list of enemy config names
    */
  private def fillEnemyQueue(enemies: Iterable[String]): Unit = {

    enemyQueue = enemies.map(name => {
      Debug.logTime(() => {
        new Enemy(name) {
          position = new Vector3f(startingPosition)
          addComponent(new PathComponent(path, (startingTile.x, startingTile.y), () => definition.speed))
        }
      }, "create enemy", this)
    }).toBuffer


  }

  /**
    * Start the next wave
    *
    * @return the next (now current) wave
    */
  private def nextWave(): Wave = {
    Debug.logTime(() => {
      if (waves.hasNext) {
        val wave = waves.next()
        fillEnemyQueue(wave.enemies)
        lastEnemy = sendNextEnemy()
        waveIndex += 1
        Events.signal(WaveUpdate(waveIndex))
        currentWave
      } else {
        Events.signal(LevelComplete(this))
        currentWave
      }
    }, "Started next wave", this)
  }

  /**
    * Construct the paths buffer based on given points and entry directions
    *
    * The path is constructed by traveling from first point along one axis until that matches the axis of the next
    * points and then doing the same for the other axis. The axis handled first is defined by the value attached to
    * the point
    *
    * Example:
    *
    * Consider points A = (0,1) and B = (4,3) on a 5x5 grid.
    *
    * # # # # #
    * A # # # #
    * # # # # #
    * # # # # B
    * # # # # #
    *
    * If the entry direction to B is set as x axis the result is the following path:
    *
    * # # # # #
    * A # # # #
    * . # # # #
    * . . . . B
    * # # # # #
    *
    * Otherwise the result is the following path:
    *
    * # # # # #
    * A . . . .
    * # # # # .
    * # # # # B
    * # # # # #
    *
    * Note that the entry direction of A doesn't affect the path leaving it in any way
    *
    * @param points list of points
    */
  def constructPaths(points: Iterable[CoordVal]): Unit = {
    var lastPoint: Option[CoordVal] = None
    points.foreach(point => {
      lastPoint.foreach(previous => {
        def addPath(x: Int, y: Int): Unit = {
          path += ((x, y))
        }

        def addXAxis(x1: Int, x2: Int, y: Int): Unit = (x2 until x1 by (if (x1 < x2) -1 else 1))
          .foreach(x => addPath(x, y))


        def addYAxis(y1: Int, y2: Int, x: Int): Unit = (y2 until y1 by (if (y1 < y2) -1 else 1))
          .foreach(y => addPath(x, y))

        point.value.toLowerCase match {
          case "x" =>
            addYAxis(point.y, previous.y, previous.x)
            addXAxis(point.x, previous.x, point.y)
          case _ =>
            addXAxis(point.x, previous.x, previous.y)
            addYAxis(point.y, previous.y, point.x)
        }

      })
      lastPoint = Some(point)
    })
  }

  def lives: Int = _lives

  def lives_=(number: Int): Unit = {
    _lives = number
    Events.signal(LivesUpdate(lives))
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: PathUpdate =>
        // Add more enemies when the previous one has moved away so that they don't stack
        if (enemyQueue.nonEmpty && event.gameObject.equals(lastEnemy) &&
          !(event.newX == startingTile.x && event.newY == startingTile.y)) {
          lastEnemy = sendNextEnemy()
        }
      case event: DeathEvent =>
        // When an enemy dies it may be the last one so we need to know
        if (enemies.contains(event.enemy)) {
          if (event.enemy == lastEnemy && enemyQueue.nonEmpty) {
            lastEnemy = sendNextEnemy()
          }
          enemies -= event.enemy
          checkNextWave()
        }
      case event: PathFinished =>
        // Remove enemies as they reach the end, start next wave when we run out of enemies
        event.gameObject match {
          case enemy: Enemy => enemies -= enemy
            removeChild(enemy)
            checkNextWave()
            lives = lives - enemy.damage
          case _ =>
        }
      case _: LevelEnd =>
        remove()
      case event: ObjectRemoved => {
        event.childObject match {
          case tower: Tower =>
            addChild(new BuildArea("tower_space") {
              position = tower.position
            })
          case _ =>
        }
      }
      case _: LevelComplete =>
        /*
         * When the level is completed displays a screen that lets the player know that has happened and returns to menu
         * after closed
         */
        addChild(new MessagePopup(windowWidth / 2 - 200, windowHeight / 2 - 100, 400, 100, "Level complete!", "Close") {
          override def onClose(): Unit = {
            Events.signal(LevelEnd())
          }
        })
      case event: LivesUpdate =>
        if (event.lives <= 0 && !Flags(GAME_OVER)) {
          Flags(GAME_OVER) = true
          addChild(new MessagePopup(windowWidth / 2 - 200, windowHeight / 2 - 100, 400, 100, "Game over!", "Close") {
            override def onClose(): Unit = {
              Flags(GAME_OVER) = false
              Events.signal(LevelEnd())
            }
          })
        }
      case _: AddDefender =>
        addChild(new Defender("stealth_plane", path, (endingTile.x, endingTile.y)))
      case _ =>
    }
  }
}

object Level {

  final val TILE_SIZE = 1.0f

  /**
    * Sent when the lives variable in level is changed
    *
    * @param lives number of lives now
    */
  case class LivesUpdate(lives: Int) extends Event

  /**
    * Sent when a new wave is started
    *
    * @param wave number of the wave
    */
  case class WaveUpdate(wave: Int) extends Event

  /**
    * Sent when the level needs to be unloaded for some reason
    */
  case class LevelEnd() extends Event

  /**
    * Sent when the level is complete, that is all the waves have passed and the player still has lives remaining
    *
    * @param level level completed
    */
  case class LevelComplete(level: Level) extends Event

}
