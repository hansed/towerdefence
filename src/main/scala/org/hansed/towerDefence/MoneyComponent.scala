package org.hansed.towerDefence

import org.hansed.engine.core.components.SimpleComponent
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.towerDefence.MoneyComponent._

/**
  * A component for storing currency
  *
  * @author Hannes Sederholm
  */
class MoneyComponent extends SimpleComponent with EventListener {

  /**
    * Amount of money stored
    */
  private var money: Int = 15

  //let everyone know there is money
  addMoney(0)

  /**
    * Change the amount of money
    *
    * @param change change in money, may be negative
    */
  def addMoney(change: Int): Unit = {
    money += change
    Events.signal(MoneyChanged(money))
  }


  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = event match {
    case event: MoneyIncreased =>
      addMoney(event.amount)
    case transaction: Transaction =>
      if (transaction.amount <= money) {
        addMoney(-transaction.amount)
        transaction.onSuccess()
      } else {
        transaction.onFailure()
      }
    case _ =>
  }
}

/**
  * Contains the event definitions for currency related events
  *
  * @author Hannes Sederholm
  */
object MoneyComponent {

  private val iterator = Iterator.from(1)

  def newTransaction: Int = iterator.next()

  /**
    * Sent when the user wants to spend or receive money
    *
    * @param amount the amount to spend
    */
  abstract case class Transaction(amount: Int) extends Event {

    /**
      * Called if the transaction was successful
      */
    def onSuccess(): Unit

    /**
      * Called if the transaction wasn't successful
      */
    def onFailure(): Unit

  }

  /**
    * Send when the player gains money trough killing enemies etc.
    *
    * @param amount amount of money gained
    */
  case class MoneyIncreased(amount: Int) extends Event

  /**
    * Sent when the amount of money is changed
    *
    * @param newAmount the amount of currency stacked after change
    */
  case class MoneyChanged(newAmount: Int) extends Event

}
