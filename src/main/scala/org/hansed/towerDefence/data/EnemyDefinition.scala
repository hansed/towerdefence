package org.hansed.towerDefence.data

/**
  * Contains information about an enemy type
  *
  * @param name   name of the enemy to be displayed in descriptions etc.
  * @param health the health of the enemy
  * @param damage the number of lives this enemy drains if it passes through
  * @param speed  how fast this enemy moves along the path
  * @param model  the model of this enemy
  * @author Hannes Sederholm
  */
case class EnemyDefinition(name: String, health: Float, damage: Int, speed: Float, model: String)
