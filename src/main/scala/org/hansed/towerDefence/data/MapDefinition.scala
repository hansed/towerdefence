package org.hansed.towerDefence.data

/**
  * Represents the area that the towers are placed on and where the enemies walk
  *
  * @param buildAreas a list of tower building areas on this map
  * @param pathPoints points that define the path, the value of it defines the axis of entry from the last path
  * @author Hannes Sederholm
  */
case class MapDefinition(width: Int, height: Int, buildAreas: Vector[CoordVal], pathPoints: Vector[CoordVal])
