package org.hansed.towerDefence.data

/**
  * Contains information about a specific level
  *
  * @param name  the name of the level, displayed on the level selection display
  * @param waves waves of enemies
  * @param lives number of lives the player has, a single enemy may drain several lives
  * @author Hannes Sederholm
  */
case class LevelDefinition(name: String, map: String, lives: Int, waves: Vector[Wave]) {

}
