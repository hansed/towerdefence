package org.hansed.towerDefence.data

/**
  * Represents a wave of enemies
  *
  * @param enemies a list of enemies spawned on this wave
  * @author Hannes Sederholm
  */
case class Wave(enemies: Vector[String])
