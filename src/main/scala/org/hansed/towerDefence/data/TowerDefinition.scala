package org.hansed.towerDefence.data

/**
  * Describes a towers properties
  *
  * @param name         name of the tower
  * @param price        how much it costs to buy one
  * @param damage       how much damage does this tower do when it hits something
  * @param speed        firing speed in shots/second
  * @param range        the max distance this tower can fire
  * @param rotateToFire should the tower face the target when it fires
  * @param projectile   the model config of the projectile the tower shoots
  * @param model        the model for this tower
  * @author Hannes Sederholm
  */
case class TowerDefinition(name: String, price: Int, damage: Float, speed: Float, range: Float,
                           firingPosition: Vector[Float], rotateToFire: Boolean, projectile: String, model: String)
