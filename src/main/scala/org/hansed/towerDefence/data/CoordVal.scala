package org.hansed.towerDefence.data

/**
  * A 2d coordinate used for saving
  *
  * @param x     coordinate
  * @param y     coordinate
  * @param value a string value attached to this coordinate
  * @author Hannes Sederholm
  */
case class CoordVal(x: Int, y: Int, value: String) {

}
