package org.hansed.towerDefence.data

import org.hansed.engine.core.resources.ConfigLoader
import spray.json._

/**
  * Deals with configuration and usage of the json library used
  *
  * @author Hannes Sederholm
  */
object SaveProtocol extends DefaultJsonProtocol {

  implicit val coordinate: RootJsonFormat[CoordVal] = jsonFormat3(CoordVal)
  implicit val enemy: RootJsonFormat[EnemyDefinition] = jsonFormat5(EnemyDefinition)
  implicit val wave: RootJsonFormat[Wave] = jsonFormat1(Wave)
  implicit val level: RootJsonFormat[LevelDefinition] = jsonFormat4(LevelDefinition)
  implicit val tower: RootJsonFormat[TowerDefinition] = jsonFormat9(TowerDefinition)
  implicit val map: RootJsonFormat[MapDefinition] = jsonFormat4(MapDefinition)

  /**
    * Loads all the enemy definitions
    */
  object EnemyDefinitionLoader extends ConfigLoader[EnemyDefinition]("game/enemies", _.convertTo[EnemyDefinition])

  /**
    * Loads the levels
    */
  object LevelDefinitionLoader extends ConfigLoader[LevelDefinition]("game/levels", _.convertTo[LevelDefinition])

  /**
    * Loads the towers
    */
  object TowerDefinitionLoader extends ConfigLoader[TowerDefinition]("game/towers", _.convertTo[TowerDefinition])

  /**
    * Loads the maps
    */
  object MapLoader extends ConfigLoader[MapDefinition]("game/maps", _.convertTo[MapDefinition])

  /**
    * Make sure that floats are actually loaded in as floats and not integers
    */
  implicit object FloatFormat extends JsonFormat[Float] {

    def write(x: Float): JsNumber = JsNumber(BigDecimal.decimal(x).rounded)

    def read(value: JsValue): Float = DefaultJsonProtocol.FloatJsonFormat.read(value)
  }

}

